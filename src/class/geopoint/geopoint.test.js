import GeoPoint from './geopoint';

test('Testing GeoPoint conversion from decimal to degree', () => {
    const latitude = 38.907192;
    const longitude = -77.036871;
    const geoPoint = new GeoPoint(longitude, latitude);

    expect(geoPoint.getLatDeg().replace('.', ',')).toBe('38° 54\' 25,89" N');
    expect(geoPoint.getLonDeg().replace('.', ',')).toBe('77° 2\' 12,74" W');
});

test('Testing GeoPoint conversion from degree to decimal', () => {
    const latitude = '38° 54\' 25.89" N';
    const longitude = '77° 2\' 12.74" W';
    const geoPoint = new GeoPoint(longitude, latitude);

    expect(geoPoint.getLatDec()).toBe(38.90719166666666);
    expect(geoPoint.getLonDec()).toBe(-77.03687222222221);
});

test('Testing GeoPoint decimal NaN', () => {
    const latitude = 238.907192;
    const longitude = -377.036871;
    const geoPoint = new GeoPoint(longitude, latitude);

    expect(geoPoint.getLonDeg()).toBe(NaN);
    expect(geoPoint.getLatDeg()).toBe(NaN);
});

test('Testing GeoPoint degree NaN', () => {
    const latitude = '238° 54\' 25.89" n';
    const longitude = '377° a156484.548984946515616548878\' 12.74" W';
    const geoPoint = new GeoPoint(longitude, latitude);

    expect(geoPoint.deg2dec(latitude, geoPoint.MAX_LAT, ['N', 'S'])).toBe(NaN);
    expect(geoPoint.deg2dec(longitude, geoPoint.MAX_LAT, ['N', 'S'])).toBe(NaN);
});
