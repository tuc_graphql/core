// Require some Node.js packages
import _ from 'lodash';
// Import the PostgreSQL package
const pg = require('pg-promise')({});

/**
 *
 * @param connectionURI: postgresql://{{ USER }}:{{ PASSWORD }}@{{ HOST }}:{{ PORT }}/{{ DATABASE }}?ssl={{ SSL }}
 * @returns {*}: Postgres connection
 */

let PGCONNECTION = {};

function connectToDB(connectionURI, schema) {
    // Return the connection
    if (_.has(PGCONNECTION, schema) && PGCONNECTION[schema] !== null) {
        return PGCONNECTION[schema];
    } else {
        PGCONNECTION[schema] = pg(connectionURI);
        return PGCONNECTION[schema];
    }
}

/**
 *
 * @param connectionURI: Postgres endpoint URI
 * @param query: Query to be executed on the Postgres backend
 * @returns {Promise<T>}: Resolved data from the postgres endpoint
 */
function resolveDataFromPG(connectionURI, schema, query) {
    const connection = connectToDB(connectionURI, schema);
    // Return data from executed query or error if occurred
    return connection
        .many(query)
        .then(data => {
            return data;
        })
        .catch(err => {
            return err;
        });
}

// Export function to be used in other JS
module.exports = {
    resolveDataFromPG,
};
