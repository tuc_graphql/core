// Determine which environment (production/development)
const PATH_ENV = process.env.NODE_ENV === 'production' ? 'dist' : 'src';

// Add the root project directory to the app module search path
import { addPath } from 'app-module-path';
addPath(__dirname);

// Require some Node.js Frameworks and the main.js from the schema folder
import express from 'express';
import Promise from 'bluebird';
import path from 'path';
import fs from 'fs';
import bodyParser from 'body-parser';

import graphql from 'express-graphql';
import schemaglue from 'schemaglue';
import readYaml from 'js-yaml';
import writeYaml from 'write-yaml';

import { makeExecutableSchema } from 'graphql-tools';
import graphqlJSON from 'graphql-type-json';

// Ignore javascript test fiiles to be glued
const ignoreNonSchemaFiles = {
    ignore: ['**/main.js', '**/*.json'],
};

// Glue schema and resolver and create an executable schema
const { schema, resolver } = schemaglue(
    PATH_ENV + '/modules',
    ignoreNonSchemaFiles
);
const executableSchema = makeExecutableSchema({
    typeDefs: schema,
    resolvers: resolver,
});

// Creating an express web server and pass the port number as an argument
const server = express();
const PORT = process.argv[2];

// TODO: LATER FOR THE REACT UI
// // Define the path of the static files
// if (PATH_ENV === 'dist') {
//     server.use('/static', express.static(path.join(__dirname, 'client', 'static')));
// } else {
//     server.use('/static', express.static(path.join(__dirname, '..', 'dist', 'client', 'static')));
// }
// // Define the path for configuration and include React
// server.use(
//     '/config',
//     (req, res) => {
//         if (PATH_ENV === 'dist') {
//             res.sendFile(path.join(__dirname, 'client', 'index.html'));
//         } else {
//             res.sendFile(path.join(__dirname, '..', 'dist', 'client', 'index.html'));
//         }
//     }
// );

// Define the root path on the web server
server.use(
    '/',
    graphql({
        schema: executableSchema,
        graphiql: true,
    })
);

// Running the server on the port (argument)
server.listen(PORT, () => {
    console.log(`Server is now running on port ${PORT}`);
});
