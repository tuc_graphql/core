// Require some Node.js packages
import _ from 'lodash';
import path from 'path';

// import functions to generate dnyamic schema
import {
    getConfigFile,
    getConfiguration,
    deleteFolder,
    createDirectorySync,
    createFileSync,
    addContentToFile,
    installDependencies,
} from './config.function';

// define static path for configuration
const MODULE_PATH = path.join(__dirname, '..', 'modules');
const MODULE_FOLDER = path.join(MODULE_PATH, '{{ schema }}');
const SCHEMA_PATH = path.join(MODULE_PATH, '{{ schema }}', 'schema');
const CONFIGURATION_PATH = path.join(MODULE_PATH, '{{ schema }}', 'config');

// Read configuration
const CONFIG_PATH = path.join(__dirname, '..', '..', 'configuration');
const CONFIG_FILES = getConfigFile(CONFIG_PATH);

// Parse configuration from JSON object to files
_.forEach(CONFIG_FILES, configFilename => {
    // Split configuration from JSON object to variables
    const schemaConfig = getConfiguration(
        path.join(CONFIG_PATH, configFilename)
    );

    const schemaName = schemaConfig.name || null,
        schemaDescription = schemaConfig.description || null,
        schemaSecrets = schemaConfig.secret || null,
        schemaField = schemaConfig.field || null,
        schemaResolver = schemaConfig.resolver || null,
        schemaSource = schemaConfig.source || null;

    if (schemaName !== null) {
        const moduleFolder = MODULE_FOLDER.replace('{{ schema }}', schemaName),
            schemaFolder = SCHEMA_PATH.replace('{{ schema }}', schemaName),
            configurationFolder = CONFIGURATION_PATH.replace(
                '{{ schema }}',
                schemaName
            );

        const mergeConfiguration = {
            secret: schemaSecrets,
            source: schemaSource,
        };

        // Create cache folders
        createDirectorySync(moduleFolder);
        createDirectorySync(schemaFolder);
        createDirectorySync(configurationFolder);

        // Add config to file
        addContentToFile(
            path.join(configurationFolder, 'config.json'),
            JSON.stringify(mergeConfiguration)
        );

        if (schemaField !== null) {
            addContentToFile(
                path.join(schemaFolder, `schema.graphql`),
                schemaField
            );
        }

        if (schemaResolver !== null) {
            // Add resolver to file
            addContentToFile(
                path.join(schemaFolder, 'resolver.js'),
                schemaResolver
            );
        }

        // Install extra dependencies if defined
        if (
            schemaSecrets !== null &&
            schemaSecrets.hasOwnProperty('DEPENDENCIES')
        ) {
            installDependencies(schemaSecrets.DEPENDENCIES);
        }
    }
});
