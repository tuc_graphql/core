# Contributing to GraphQL - DAIMON Project (TU Clausthal)

<!-- START doctoc generated TOC please keep comment here to allow auto update -->

<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

*   [Introduction](#introduction)
*   [Folder Structure](#folder-structure)
*   [How to Contribute](#how-to-contribute)
*   [Contribution Environment](#contribution-environment)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Introduction

This document clarify the project structure and the contribution process.
It also gives an overview for the softwares and packages used for the development.

## Folder Structure

The project is divided into two folders `src` and `docs`. The `src` contains the core files to run the server and
the `docs` contains the documentation on how to install and use the server to request the data.

`src` is divided into:

*   `filter` contains the schema filters, which may be applied on its fields.
*   `function` contains the schema function functions, which request the data from the sources.
*   `graphql` contains the schema definition and resolver.
*   `script` contains extra scripts, which may be used by the filters and/or functions.

`docs` is divided into:

*   `archetypes` contains the basic structure of each documentation section.
*   `content` contains the actual documentation divided into categories.
*   `static` contains the images and pdf.
*   `themes` contains a documentation theme.

## How to Contribute

### Contribute to Project

Adding new schema requires creating a folder in `graphql` with its name.
The folder must contain `schema.graphql` and `resolver.js`.
The definition of the schema has to be implemented in `schema.graphql`.
Likewise the resolver has to be implemented in `resolver.js`

### Contribute to Documentation

Adding new section to the documentation requires creating a file in `archetypes` with its name.
Its content might be copied from other section files from `archetypes`.
Add new page to the created category using the following command:

```sh
# Replace {SECTION} with the section name
# Replace {FILENAME} with the desired file name
hugo new {SECTION}/{FILENAME}
```

## Contribution Environment

### Softwares and Platforms

Current softwares and platforms:

*   [WebStorm](https://www.jetbrains.com/webstorm/) version 2018.1
*   [NodeJS](https://nodejs.org) version 8.11.1 TLS
*   [NPM](https://www.npmjs.com/) version 6.0.0

### NodeJS Packages

The following NodeJS packages are used to run syntax check, format and test the project code.

*   [ESLint](https://eslint.org/) version 4.19.1
*   [Prettier](https://prettier.io/) version 1.10.2
*   [Jest](https://facebook.github.io/jest/) version 22.4.3

ESLint and Jest are integrated in the CI pipelines on GitLab as well.

### Code Formatter

Prettier is used to format the project code. For this purpose, following configuration is used.

*   4 spaces for indentation (no tabs)
*   80 character line length
*   Prefer `'` over `"`
*   Use semicolons
*   Trailing commas

### Code Testing

Jest is used to test `filters`, `functions` and `scripts`.
It is also possible to run code coverage for the written tests.
