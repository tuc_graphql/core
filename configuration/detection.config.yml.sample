# Unique identifier to the schema (OBLIGATORY)
name: detection
# schema description (OPTIONAL)
description: Locations of positive detections of chemical warfare agents in sediments samples
# Secrets to be save in the schema configuration file "config/config.json" (OBLIGATORY)
secret:
# Schema definition in GraphQL Syntax (OBLIGATORY)
field: >
  type detectionType {
      _ID: String
      objectID: String
      name: String
      details: String
      latitude: Float
      longitude: Float
      hashValue: ID
      source: String
  }

  type Query {
      detection: [detectionType]
  }
# Resolver for the data(OBLIGATORY)
resolver: >
  // Import general parser for RESTFul endpoints

  import { resolveDataFromREST, } from 'class/parser/RESTParser.main';

  // Import schema configuration from file and save it in variable CONFIG

  import CONFIG from 'modules/detection/config/config.json';

  // Function to parse data from source URL

  function detectionParser(CONFIG, schemaName) {
        const sources = CONFIG['source'];
        let promises = [];

        for (let i in sources) {
            // Save requested data (Promises) in array
            promises.push(
                resolveDataFromREST(
                    encodeURI(sources[i].url),
                    sources[i]['mapping'][schemaName] || null,
                    sources[i]['resolve'][schemaName] || null,
                    sources[i]['dataLocation'][schemaName] || null
                )
            );
        }

        // Resolve promises and return the data

        return Promise.all(promises).then(responds => {
            let data = [];
            for (let i = 0; i < responds.length; i++) {
                // Push results to one array
                for (let j = 0; j < responds[i].length; j++) {
                    data.push(responds[i][j]);
                }
            }
            return data;
        });
    }

  //  Define the queries resolvers with help of the general parser function for REST

  exports.resolver = {
      Query: {
          detection(root) {
              // Pass the schema configuration and the requested query

              return detectionParser(CONFIG, 'detection');
          },
      },
  };
# Sources to request the data (OPTIONAL)
source:
  # Saved as array (OBLIGATORY)
  1:
    name: chemsea
    url: http://153.19.108.112/arcgis/rest/services/chemsea/MapServer/1/query?f=json&where=(1=1)&returnGeometry=false&spatialRel=esriSpatialRelIntersects&outFields=*&orderByFields=objectid ASC&outSR=102100&resultOffset=0
    # Mapping between REST endpoint fields (LEFT) and schema fileds (RIGHT)
    mapping:
      # Query name as defined in the schema (OBLIGATORY)
      detection:
        objectid: objectID
        name: name
        punpch_p_opis: details
        punpch_p_szer_wgs84: latitude
        punpch_p_dl_wgs84: longitude
    # Fixed values or Filter functions for requested data (optional)
    resolve:
      # Query name as defined in the schema (OBLIGATORY)
      detection:
        # Always resolve _ID to 1 from this source
        _ID: 1
        name: Sulfur mustard
        source: chemsea
        # Pass hashValue value to a filter function
        hashValue:
          # Type of filter is a function (OBLIGATORY)
          type: function
          # Function body without return (OBLIGATORY)
          definition: generateSipHash(data['_ID'], data['objectID'], data['source']);
        longitude:
          type: function
          definition: convertWGS84ToDecimal(data['longitude'], data['latitude'], true).longitude
        latitude:
          type: function
          definition: convertWGS84ToDecimal(data['longitude'], data['latitude'], true).latitude
    # Location of requested data (OPTIONAL)
    dataLocation:
      detection: features->attributes
  2:
    name: chemsea
    url: http://153.19.108.112/arcgis/rest/services/chemsea/MapServer/2/query?f=json&where=(1=1)&returnGeometry=false&spatialRel=esriSpatialRelIntersects&outFields=*&orderByFields=objectid ASC&outSR=102100&resultOffset=0
    mapping:
      detection:
        objectid: objectID
        name: name
        punpch_p_opis: details
        punpch_p_szer_wgs84: latitude
        punpch_p_dl_wgs84: longitude
    resolve:
      detection:
        _ID: 2
        name: Adamsite
        source: chemsea
        hashValue:
          type: function
          definition: generateSipHash(data['_ID'], data['objectID'], data['source']);
        longitude:
          type: function
          definition: convertWGS84ToDecimal(data['longitude'], data['latitude'], true).longitude
        latitude:
          type: function
          definition: convertWGS84ToDecimal(data['longitude'], data['latitude'], true).latitude
    dataLocation:
      detection: features->attributes
  3:
    name: chemsea
    url: http://153.19.108.112/arcgis/rest/services/chemsea/MapServer/3/query?f=json&where=(1=1)&returnGeometry=false&spatialRel=esriSpatialRelIntersects&outFields=*&orderByFields=objectid ASC&outSR=102100&resultOffset=0
    mapping:
      detection:
        objectid: objectID
        name: name
        punpch_p_opis: details
        punpch_p_szer_wgs84: latitude
        punpch_p_dl_wgs84: longitude
    resolve:
      detection:
        _ID: 3
        name: Clark
        source: chemsea
        hashValue:
          type: function
          definition: generateSipHash(data['_ID'], data['objectID'], data['source']);
        longitude:
          type: function
          definition: convertWGS84ToDecimal(data['longitude'], data['latitude'], true).longitude
        latitude:
          type: function
          definition: convertWGS84ToDecimal(data['longitude'], data['latitude'], true).latitude
    dataLocation:
      detection: features->attributes
  4:
    name: chemsea
    url: http://153.19.108.112/arcgis/rest/services/chemsea/MapServer/4/query?f=json&where=(1=1)&returnGeometry=false&spatialRel=esriSpatialRelIntersects&outFields=*&orderByFields=objectid ASC&outSR=102100&resultOffset=0
    mapping:
      detection:
        objectid: objectID
        name: name
        punpch_p_opis: details
        punpch_p_szer_wgs84: latitude
        punpch_p_dl_wgs84: longitude
    resolve:
      detection:
        _ID: 4
        name: Triphenylarsine
        source: chemsea
        hashValue:
          type: function
          definition: generateSipHash(data['_ID'], data['objectID'], data['source']);
        longitude:
          type: function
          definition: convertWGS84ToDecimal(data['longitude'], data['latitude'], true).longitude
        latitude:
          type: function
          definition: convertWGS84ToDecimal(data['longitude'], data['latitude'], true).latitude
    dataLocation:
      detection: features->attributes
  5:
    name: chemsea
    url: http://153.19.108.112/arcgis/rest/services/chemsea/MapServer/5/query?f=json&where=(1=1)&returnGeometry=false&spatialRel=esriSpatialRelIntersects&outFields=*&orderByFields=objectid ASC&outSR=102100&resultOffset=0
    mapping:
      detection:
        objectid: objectID
        name: name
        punpch_p_opis: details
        punpch_p_szer_wgs84: latitude
        punpch_p_dl_wgs84: longitude
    resolve:
      detection:
        _ID: 5
        name: Phenyldichloroarsine
        source: chemsea
        hashValue:
          type: function
          definition: generateSipHash(data['_ID'], data['objectID'], data['source']);
        longitude:
          type: function
          definition: convertWGS84ToDecimal(data['longitude'], data['latitude'], true).longitude
        latitude:
          type: function
          definition: convertWGS84ToDecimal(data['longitude'], data['latitude'], true).latitude
    dataLocation:
      detection: features->attributes
  6:
    name: chemsea
    url: http://153.19.108.112/arcgis/rest/services/chemsea/MapServer/6/query?f=json&where=(1=1)&returnGeometry=false&spatialRel=esriSpatialRelIntersects&outFields=*&orderByFields=objectid ASC&outSR=102100&resultOffset=0
    mapping:
      detection:
        objectid: objectID
        name: name
        punpch_p_opis: details
        punpch_p_szer_wgs84: latitude
        punpch_p_dl_wgs84: longitude
    resolve:
      detection:
        _ID: 6
        name: Chloroacetophenone
        source: chemsea
        hashValue:
          type: function
          definition: generateSipHash(data['_ID'], data['objectID'], data['source']);
        longitude:
          type: function
          definition: convertWGS84ToDecimal(data['longitude'], data['latitude'], true).longitude
        latitude:
          type: function
          definition: convertWGS84ToDecimal(data['longitude'], data['latitude'], true).latitude
    dataLocation:
      detection: features->attributes
  7:
    name: chemsea
    url: http://153.19.108.112/arcgis/rest/services/chemsea/MapServer/7/query?f=json&where=(1=1)&returnGeometry=false&spatialRel=esriSpatialRelIntersects&outFields=*&orderByFields=objectid ASC&outSR=102100&resultOffset=0
    mapping:
      detection:
        objectid: objectID
        name: name
        punpch_p_opis: details
        punpch_p_szer_wgs84: latitude
        punpch_p_dl_wgs84: longitude
    resolve:
      detection:
        _ID: 7
        name: Lewisite I
        source: chemsea
        hashValue:
          type: function
          definition: generateSipHash(data['_ID'], data['objectID'], data['source']);
        longitude:
          type: function
          definition: convertWGS84ToDecimal(data['longitude'], data['latitude'], true).longitude
        latitude:
          type: function
          definition: convertWGS84ToDecimal(data['longitude'], data['latitude'], true).latitude
    dataLocation:
      detection: features->attributes
  8:
    name: chemsea
    url: http://153.19.108.112/arcgis/rest/services/chemsea/MapServer/8/query?f=json&where=(1=1)&returnGeometry=false&spatialRel=esriSpatialRelIntersects&outFields=*&orderByFields=objectid ASC&outSR=102100&resultOffset=0
    mapping:
      detection:
        objectid: objectID
        name: name
        punpch_p_opis: details
        punpch_p_szer_wgs84: latitude
        punpch_p_dl_wgs84: longitude
    resolve:
      detection:
        _ID: 8
        name: Lewisite II
        source: chemsea
        hashValue:
          type: function
          definition: generateSipHash(data['_ID'], data['objectID'], data['source']);
        longitude:
          type: function
          definition: convertWGS84ToDecimal(data['longitude'], data['latitude'], true).longitude
        latitude:
          type: function
          definition: convertWGS84ToDecimal(data['longitude'], data['latitude'], true).latitude
    dataLocation:
      detection: features->attributes
  9:
    name: chemsea
    url: http://153.19.108.112/arcgis/rest/services/chemsea/MapServer/9/query?f=json&where=(1=1)&returnGeometry=false&spatialRel=esriSpatialRelIntersects&outFields=*&orderByFields=objectid ASC&outSR=102100&resultOffset=0
    mapping:
      detection:
        objectid: objectID
        name: name
        punpch_p_opis: details
        punpch_p_szer_wgs84: latitude
        punpch_p_dl_wgs84: longitude
    resolve:
      detection:
        _ID: 9
        name: Total arsenic
        source: chemsea
        hashValue:
          type: function
          definition: generateSipHash(data['_ID'], data['objectID'], data['source']);
        longitude:
          type: function
          definition: convertWGS84ToDecimal(data['longitude'], data['latitude'], true).longitude
        latitude:
          type: function
          definition: convertWGS84ToDecimal(data['longitude'], data['latitude'], true).latitude
    dataLocation:
      detection: features->attributes
  10:
    name: helcom
    url: http://maps.helcom.fi/arcgis104/rest/services/MADS/Pressures/MapServer/73/query?f=pjson&where=(1=1)&returnGeometry=false&spatialRel=esriSpatialRelIntersects&outFields=*&orderByFields=OBJECTID ASC&outSR=102100&resultOffset=0
    mapping:
      detection:
        ID: _ID
        OBJECTID: objectID
        Active_age: name
        Details: details
        Lat_dd: latitude
        Long_dd: longitude
    resolve:
      detection:
        source: helcom
        hashValue:
          type: function
          definition: generateSipHash(data['_ID'], data['objectID'], data['source']);
    dataLocation:
      detection: features->attributes