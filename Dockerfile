FROM node:carbon-alpine
WORKDIR /app
COPY package.json /app/package.json
COPY package-lock.json /app/package-lock.json
RUN npm i npm@5.10.0 -g
RUN npm install --production
COPY . /app/.
EXPOSE 4000
RUN npm run create:sample:modules
RUN npm run create:modules
RUN npm run build:prettier
CMD ["npm", "run", "start", "$npm_package_config_port"]