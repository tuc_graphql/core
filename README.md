# GraphQL - DAIMON Project (TU Clausthal)

<!-- START doctoc generated TOC please keep comment here to allow auto update -->

<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

-   [Badges](#badges)
-   [About the Project](#about-the-project)
-   [Documentation](#documentation)
-   [Project Environments](#project-environments)
-   [How to Install](#how-to-install)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Badges

[![pipeline status](https://gitlab.com/tuc_graphql/core/badges/master/pipeline.svg)](https://gitlab.com/tuc_graphql/core/commits/master)
[![coverage report](https://gitlab.com/tuc_graphql/core/badges/master/coverage.svg)](https://tuc_graphql.gitlab.io/core/coverage/lcov-report/index.html)
[![release](https://img.shields.io/badge/release-v1.0.0-brightgreen.svg)](https://gitlab.com/tuc_graphql/core/tags/v0.6)

[![node](https://img.shields.io/badge/node-v8.11.3-blue.svg)](https://docs.npmjs.com/)
[![npm](https://img.shields.io/badge/npm-v5.10.0-blue.svg)](https://docs.npmjs.com/)
[![license](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/tuc_graphql/core/blob/master/LICENSE)

[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)
[![jest](https://facebook.github.io/jest/img/jest-badge.svg)](https://github.com/facebook/jest)

## About the Project

Retrieve data with help of [GraphQL](http://graphql.org/learn/).

## Documentation

For the **Documentation** and **How to Use** please visit one of the following websites:

-   [hosted wiki with Hugo](https://tuc_graphql.gitlab.io/core)
-   [project wiki on GitLab](https://gitlab.com/tuc_graphql/core/wikis/home)

## Project Environments

To test the latest build, please visit one of the following links:

-   [production](https://production-graphql-core.herokuapp.com)
-   [staging](https://staging-graphql-core.herokuapp.com)

## How to Install

### Manual Installation

-   Clone repository:

```console
git clone https://gitlab.com/tuc_graphql/core.git
```

-   Switch branch:

```sh
# Replace {BRANCH} with master (production) or staging (developement)
git checkout {BRANCH}
```

-   Install dependencies:

```console
npm install
```

-   Create sample configuration:

```console
npm run create:sample:modules
```

-   Build modules from configuration files:

```console
npm run create:modules
```

-   Convert javascript ECMAScript 6 (ES6) to ECMAScript, 5th Edition (ES5):

```sh
npm run build
# Code Formatter (more human-readable)
npm run prettier
```

-   Start server:

```sh
# Replace {PORT} with any desired port number, e.g. 4000
npm run start {PORT}
```

### Docker Installation

-   Login to [GitLab container registry](https://gitlab.com/help/user/project/container_registry):

```console
docker login registry.gitlab.com
```

-   Pull image for branch:

```sh
# Replace {NAME} with production or staging and {TAG} with latest
docker pull registry.gitlab.com/tuc_graphql/core/{NAME}:{tag}
```

-   Pull image for tag:

```sh
# Replace {TAG} with tag number, e.g. v0.2
docker pull registry.gitlab.com/tuc_graphql/core:{tag}
```

-   Run container:

```sh
# Replace {HOST_PORT} with any desired port number, e.g. 4000
# Replace {NAME} with any desired name
# Replace {IMAGE} with image name pulled from GitLab
docker run --detach --publish {HOST_PORT}:4000 --rm --name {NAME} {IMAGE}
# Container can be accessed localy on http://localhost:{HOST_PORT}
```
