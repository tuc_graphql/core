---
title: "Installation"
date: 2018-01-24T01:06:16+01:00
author: "Abram Lawendy"
menu: main
type: installation
weight: 30
draft: false
---

### Table of contents

<!-- START doctoc generated TOC please keep comment here to allow auto update -->

<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

-   [Manual Installation](#manual-installation)
-   [Docker Installation](#docker-installation)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

### Manual Installation

-   Clone repository:

```console
git clone https://gitlab.com/tuc_graphql/core.git
```

-   Switch branch:

```sh
# Replace {BRANCH} with master (production) or staging (developement)
git checkout {BRANCH}
```

-   Install dependencies:

```console
npm install
```

-   Create sample configuration:

```console
npm run create:sample:modules
```

-   Build modules from configuration files:

```console
npm run create:modules
```

-   Convert javascript ECMAScript 6 (ES6) to ECMAScript, 5th Edition (ES5):

```sh
npm run build
# Code Formatter (more human-readable)
npm run prettier
```

-   Start server:

```sh
# Replace {PORT} with any desired port number, e.g. 4000
npm run start {PORT}
```

### Docker Installation

-   Login to [GitLab container registry](https://gitlab.com/help/user/project/container_registry):

```console
docker login registry.gitlab.com
```

-   Pull image for branch:

```sh
# Replace {NAME} with production or staging and {TAG} with latest
docker pull registry.gitlab.com/tuc_graphql/core/{NAME}:{tag}
```

-   Pull image for tag:

```sh
# Replace {TAG} with tag number, e.g. v0.2
docker pull registry.gitlab.com/tuc_graphql/core:{tag}
```

-   Run container:

```sh
# Replace {HOST_PORT} with any desired port number, e.g. 4000
# Replace {NAME} with any desired name
# Replace {IMAGE} with image name pulled from GitLab
docker run --detach --publish {HOST_PORT}:4000 --rm --name {NAME} {IMAGE}
# Container can be accessed localy on http://localhost:{HOST_PORT}
```
