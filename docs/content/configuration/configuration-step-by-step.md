---
title: "Configuration > Step by Step"
date: 2018-05-21T17:49:32+02:00
author: "Abram Lawendy"
menu: main
type: use
weight: 50
draft: false
---

### Table of contents

<!-- START doctoc generated TOC please keep comment here to allow auto update -->

<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

-   [Read Carefully](#read-carefully)
-   [Configuration Breakdown](#configuration-breakdown)
    -   [Schema Name](#schema-name)
    -   [Secret](#secret)
    -   [Field](#field)
    -   [Resolver](#resolver)
    -   [Source](#source)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

### Read Carefully

Every schema should be defined in its own configuration file.
All configuration files must be saved in the `configuration` folder and
the filename must end with `.yml` for example `forecast.config.yml`

The latest project build contains 4 configuration samples `detection.config.yml.sample`, `forecast.config.yml.sample`,
`ecosystemdata.config.yml.sample` and `munition.config.yml.sample`

To create productive schema configuration from the sample:

```console
npm run create:sample:modules
npm run create:modules
npm run build:prettier
```

### Configuration Breakdown

Every schema configuration file should contains the following keys:

-   name
-   secret _(optional)_
-   filed
-   function _(optional)_
-   resolver
-   filter _(optional)_
-   source _(optional)_

#### Schema Name

`name` is a key which holds a unique identifier for the schema. A folder with this name will be created/overridden inside the `modules` folder.
The `modules` folder contains the schema definition, resolver, filter and configuration.

Example:

```yaml
# Unique identifier to the schema (OBLIGATORY)
name: detection
# schema description (OPTIONAL)
description: Locations of positive detections of chemical warfare agents in sediments samples
```

#### Secret

`secret` is a key which holds secrets for the schema, for example `access token`.
The variable will be saved in a `.json` file inside a `config` folder.

Example:

```yaml
# Secrets to be save in the schema configuration file "config/config.json" (OBLIGATORY)
secret:
  TOKEN:
  HOST:
  USER:
  PASSWORD:
```

For example:

```javascript
// Access the secret variable inside any JS file
// Replace {VARIABLE} with a variable name and {SCHEMA} with the schema name
import { VARIABLE } from 'modules/{SCHEMA}/config/config.json';
```

#### Field

`field` is a key which holds the schema definition in a GraphQL syntax.
For more information on how to define schema in GraphQL, please visit the following [website](https://graphql.org/learn/schema/).

Example:

```graphql
# A simple type definition
type detectionType {
    _ID: String
    objectID: String
    name: String
    details: String
    latitude: Float
    longitude: Float
    hashValue: ID
    source: String
}

# Any requested query should be written inside the Query type
type Query {
    detection: [detectionType]
}
```

#### Resolver

`resolver` is a key which holds a **JS script** to resolve the schema to its data.

Example:

```YAML
# Resolver for the data(OBLIGATORY)
resolver: >
  // Import general parser for RESTFul endpoints

  import { resolveDataFromREST, } from 'class/parser/RESTParser.main';

  // Import schema configuration from file and save it in variable CONFIG

  import CONFIG from 'modules/detection/config/config.json';

  // Function to parse data from source URL

  function detectionParser(CONFIG, schemaName) {
        const sources = CONFIG['source'];
        let promises = [];

        for (let i in sources) {
            // Save requested data (Promises) in array
            promises.push(
                resolveDataFromREST(
                    encodeURI(sources[i].url),
                    sources[i]['mapping'][schemaName] || null,
                    sources[i]['resolve'][schemaName] || null,
                    sources[i]['dataLocation'][schemaName] || null
                )
            );
        }

        // Resolve promises and return the data

        return Promise.all(promises).then(responds => {
            let data = [];
            for (let i = 0; i < responds.length; i++) {
                // Push results to one array
                for (let j = 0; j < responds[i].length; j++) {
                    data.push(responds[i][j]);
                }
            }
            return data;
        });
    }

  //  Define the queries resolvers with help of the general parser function for REST

  exports.resolver = {
      Query: {
          detection(root) {
              // Pass the schema configuration and the requested query

              return detectionParser(CONFIG, 'detection');
          },
      },
  };
```

#### Source

Every source should contains the following keys:

-   name _(OPTIONAL)_
-   url _(OBLIGATORY)_
-   mapping _(OBLIGATORY)_
-   resolve _(OBLIGATORY)_
-   dataLocation _(OBLIGATORY)_

##### Name

`name` is a key which holds the name of the source where the data is requested.

##### URL

`url` is a key which holds the URL from which the data should be requested.

##### Mapping

`mapping` is a key which holds a mapping between the result of the requested data and defined schema.

Example:

```yaml
# Mapping between REST endpoint fields (LEFT) and schema fileds (RIGHT)
mapping:
  # Query name as defined in the schema (OBLIGATORY)
  detection:
    objectid: objectID
    name: name
    punpch_p_opis: details
    punpch_p_szer_wgs84: latitude
    punpch_p_dl_wgs84: longitude
```

##### Resolve

`resolve` is a key which holds a static resolve for fields which will be created/overridden in the result of the requested data.

Example:

```yaml
# Fixed values or Filter functions for requested data (optional)
resolve:
  # Query name as defined in the schema (OBLIGATORY)
  detection:
    # Always resolve _ID to 1 from this source
    _ID: 1
    name: Sulfur mustard
    source: chemsea
    # Pass hashValue value to a filter function
    hashValue:
      # Type of filter is a function (OBLIGATORY)
      type: function
      # Function body without return (OBLIGATORY)
      definition: generateSipHash(data['_ID'], data['objectID'], data['source']);
    longitude:
      type: function
      definition: convertWGS84ToDecimal(data['longitude'], data['latitude'], true).longitude
    latitude:
      type: function
      definition: convertWGS84ToDecimal(data['longitude'], data['latitude'], true).latitude
```

##### Data Location

`dataLocation` is a key which holds the location of requested data in returned data from source.

Example:

```yaml
# Location of requested data (OPTIONAL)
dataLocation:
  detection: features->attributes
```
