---
title: "DAIMON Project"
date: 2016-04-23T15:21:22+02:00
author: "Abram Lawendy"
menu: main
type: homepage
weight: 10
draft: false
---

## GraphQL - DAIMON Project (TU Clausthal)

### Table of contents

<!-- START doctoc generated TOC please keep comment here to allow auto update -->

<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

-   [About the Project](#about-the-project)
-   [Documentation](#documentation)
-   [Project Environments](#project-environments)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

### About the Project

The aim of this project is the integration of heterogeneous data using [GraphQL](http://graphql.org/learn/).
It can preform data requests from JSON- and XML-based REST service and from PostgreSQL database server.

### Documentation

For the **Documentation** and **How to Use** please visit one of the following websites:

-   [hosted wiki with Hugo](https://tuc_graphql.gitlab.io/core)
-   [project wiki on GitLab](https://gitlab.com/tuc_graphql/core/wikis/home)

### Project Environments

To test the latest build, please visit one of the following links:

-   [production](https://production-graphql-core.herokuapp.com)
-   [staging](https://staging-graphql-core.herokuapp.com)
