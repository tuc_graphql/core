---
title: "Data Model"
date: 2018-01-25T00:43:42+01:00
author: "Abram Lawendy"
menu: main
type: data
weight: 60
draft: false
---

### Table of contents

<!-- START doctoc generated TOC please keep comment here to allow auto update -->

<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

-   [Database Data Model](#database-data-model)
-   [GraphQL Data Model](#graphql-data-model)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

### Database Data Model

The following picture represents the source data model for the project.
The corresponding PDF file can be downloaded from [here](../../pdf/source-data-model.pdf).
![source-datamodel-png](../../images/source-data-model.png)

### GraphQL Data Model

The following picture represents the implemented GraphQL data model for the project
The corresponding PDF file can be downloaded from [here](../../pdf/graphql-data-model.pdf).
![graphql-datamodel-png](../../images/graphql-data-model.png)
