---
title: "Introduction"
date: 2018-02-16T20:35:44+01:00
author: "Abram Lawendy"
menu: main
type: use
weight: 20
draft: false
---

### Table of contents

<!-- START doctoc generated TOC please keep comment here to allow auto update -->

<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

-   [GraphQL](#graphql)
-   [Read Carefully](#read-carefully)
-   [Schemas and Types](#schemas-and-types)
-   [Root fields & resolvers](#root-fields-&-resolvers)
-   [Queries and Mutations](#queries-and-mutations)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

### GraphQL

GraphQL is a query language for APIs and a runtime for fulfilling those queries with your existing data.
GraphQL provides a complete and understandable description of the data in your API,
gives clients the power to ask for exactly what they need and nothing more, makes it easier to evolve APIs over time,
and enables powerful developer tools. At its simplest, GraphQL is about asking for specific fields on objects.<br />
-- [GraphQL](https://graphql.org/)

### Read Carefully

In order to create a module configuration file, one should have the basic understand on how to create GraphQL **schemas**, **types** and **resolver**
as well as how to execute GraphQL **queries**.

## Schemas and Types

It is very important to understand how to create schemas with **default** and **custom** scalar types.
This is a key element to build a model for the project using a `configuration` file or directly in the `modules` folder.

The detailed documentation on how to create GraphQL schemas and types
can be found in [GraphQL Schemas and Types](https://graphql.org/learn/schema/#type-system).

## Root fields & resolvers

A so called resolver function should be implemented to request the data of the defined schema from its sources.

The detailed documentation on how to create GraphQL resolvers
can be found in [Root fields & resolvers](https://graphql.org/learn/execution/#root-fields-resolvers).

## Queries and Mutations

The following example shows how to request data using the GraphQL query shape.
The response is a JSON object that has the same shape as the query.

The detailed documentation on how to query a GraphQL server
can be found in [Queries and Mutations](https://graphql.org/learn/queries/).

{{< admonition title="simple query" type="note" >}}

```sh
Request:                  Response:

{                   |     {
  hero {            |       "data": {
    name            |          "hero": {
    friends {       |             "name": "R2-D2",
      name          |             "friends": [
    }               |               {
  }                 |                 "name": "Luke Skywalker"
}                   |               },
                    |               {
                    |                 "name": "Han Solo"
                    |               }
                    |             ]
                    |          }
                    |        }
                    |      }
```

{{< /admonition >}}
