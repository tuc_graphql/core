## Deployment **{X.x.x}**

Deploy to production and update version to **{X.x.x}**

### Merge `Staging` with `Master`

This MR merges the **`staging`** branch with with the **`master`** branch and updates the version to **`{X.x.x}`**

This deploys the latest releases on the productive server.

Please refer to <https://staging-graphql-core.herokuapp.com> for the last staging release
and <https://production-graphql-core.herokuapp.com> for the last master deployment.

### Resolved Issues

<!-- Mention the issue(s) this MR closes or is related to -->

Closes #

### Labels and Milestones

<!-- Mention the Label(s) and Milestone(s) to those issues as well -->

/milestone %

/label ~
