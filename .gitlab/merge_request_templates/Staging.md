## Deploy to Staging

Deploy to staging for review.

### Merge `{BRANCH}` with `Staging`

<!-- Mention the branch name, which should be merged with staging -->

This MR merges the **`{BRANCH}`** branch with with the **staging** branch.

The staging server <https://staging-graphql-core.herokuapp.com> should be reviewd after accepting this MR.

### Resolved Issues

<!-- Mention the issue(s) this MR closes or is related to -->

Closes #

### Labels and Milestones

<!-- Mention the Label(s) and Milestone(s) to those issues as well -->

/milestone %

/label ~
